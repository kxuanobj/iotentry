/**
 * @fileoverview gRPC-Web generated client stub for iot.protobuf.hub
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var iot_protobuf_device_pb = require('../../../iot/protobuf/device_pb.js')
const proto = {};
proto.iot = {};
proto.iot.protobuf = {};
proto.iot.protobuf.hub = require('./service_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.iot.protobuf.hub.HubServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.iot.protobuf.hub.HubServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!proto.iot.protobuf.hub.HubServiceClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.iot.protobuf.hub.HubServiceClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.iot.protobuf.hub.RegisterRequest,
 *   !proto.iot.protobuf.hub.RegisterResult>}
 */
const methodInfo_Register = new grpc.web.AbstractClientBase.MethodInfo(
  proto.iot.protobuf.hub.RegisterResult,
  /** @param {!proto.iot.protobuf.hub.RegisterRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.iot.protobuf.hub.RegisterResult.deserializeBinary
);


/**
 * @param {!proto.iot.protobuf.hub.RegisterRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.iot.protobuf.hub.RegisterResult)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.iot.protobuf.hub.RegisterResult>|undefined}
 *     The XHR Node Readable Stream
 */
proto.iot.protobuf.hub.HubServiceClient.prototype.register =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/iot.protobuf.hub.HubService/Register',
      request,
      metadata,
      methodInfo_Register,
      callback);
};


/**
 * @param {!proto.iot.protobuf.hub.RegisterRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.iot.protobuf.hub.RegisterResult>}
 *     The XHR Node Readable Stream
 */
proto.iot.protobuf.hub.HubServicePromiseClient.prototype.register =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.register(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.iot.protobuf.hub.ListRequest,
 *   !proto.iot.protobuf.hub.ListResult>}
 */
const methodInfo_List = new grpc.web.AbstractClientBase.MethodInfo(
  proto.iot.protobuf.hub.ListResult,
  /** @param {!proto.iot.protobuf.hub.ListRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.iot.protobuf.hub.ListResult.deserializeBinary
);


/**
 * @param {!proto.iot.protobuf.hub.ListRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.iot.protobuf.hub.ListResult)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.iot.protobuf.hub.ListResult>|undefined}
 *     The XHR Node Readable Stream
 */
proto.iot.protobuf.hub.HubServiceClient.prototype.list =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/iot.protobuf.hub.HubService/List',
      request,
      metadata,
      methodInfo_List,
      callback);
};


/**
 * @param {!proto.iot.protobuf.hub.ListRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.iot.protobuf.hub.ListResult>}
 *     The XHR Node Readable Stream
 */
proto.iot.protobuf.hub.HubServicePromiseClient.prototype.list =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.list(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


module.exports = proto.iot.protobuf.hub;

