import Vue from 'vue'
import App from './App.vue'
import HostTable from "./js/HostTable"

import {grpc} from "grpc-web-client"
import {EchoServicePromiseClient} from "./_proto/iot/protobuf/echo/service_grpc_web_pb"
import {EchoRequest, EchoResponse} from "./_proto/iot/protobuf/echo/service_pb"

new Vue({
    el: '#app',
    render: h => h(App)
});

(async function () {
    const host = "http://127.0.0.1:9090/2";
    let svc = new EchoServicePromiseClient(host);
    let req = new EchoRequest();
    req.setData((new TextEncoder()).encode("Browser!"));
    let r = await svc.echo(req);
    console.log(r.toObject());
})();