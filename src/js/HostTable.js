import Device from "./Device";

const deviceCache = new Map();

export default class HostTable {
    constructor() {
    }

    static async get(uuid) {
        return deviceCache.get(uuid);
    }
}