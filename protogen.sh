#!/bin/bash
cd $(dirname $0)
rm -rf src/_proto/
mkdir -p src/_proto
export PATH="$PATH:$PWD/node_modules/.bin"

protoc \
	-I ./proto \
	--js_out=import_style=commonjs,binary:src/_proto \
	--grpc-web_out=import_style=commonjs,mode=grpcwebtext:src/_proto \
	./proto/iot/protobuf/device.proto \
	./proto/iot/protobuf/hub/service.proto \
	./proto/iot/protobuf/echo/service.proto \

